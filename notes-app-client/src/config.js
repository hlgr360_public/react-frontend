
export default {
    MAX_ATTACHMENT_SIZE: 5000000,
    
    s3: {
      BUCKET: "adctrl-direct-uploads"
    },

    apiGateway: {
      URL: "https://xrexmg8iw4.execute-api.eu-west-1.amazonaws.com/prod",
      REGION: "eu-west-1"
    },

    cognito: {
      USER_POOL_ID: "eu-west-1_IBpPUw7o9",
      APP_CLIENT_ID: "3h3d4rj2gv3o2v5ld5orohe7k1",
      REGION: "eu-west-1",
      IDENTITY_POOL_ID: "eu-west-1:7528d446-5ef1-47a0-ab06-36fc84397b1c"
    }
  };